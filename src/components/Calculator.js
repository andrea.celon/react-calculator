import React from 'react';
import Results from './Results';
import Keyboard from './Keyboard';

class Calculator extends React.Component {
    state = {
        
        firstNum: [],
        secondNum: [],
        operator: '',
        result: ''
    };

    renderNumb = (numb) => {
        if(this.state.result === '') {
            if (this.state.operator === '') {
                this.setState({firstNum: numb});
            } else {
                this.setState({secondNum: numb});
            }
        }else{
            this.setState({
                firstNum: [numb],
                secondNum: [],
                operator: '',
                result: ''
            });
        }
    };

    renderOp = (op) => {
        if(this.state.result === '') {
            if (op === '=') {
                this.setState({result: '= ' + this.calculate()});
            }else
                this.setState({operator:op})
        }else{
            const oldResult = this.calculate();
             this.setState({
                firstNum: [oldResult],
                secondNum: [],
                operator: op,
                result: ''
            });

        }
    };

    calculate = () => {
        const num1 = parseInt(this.state.firstNum.join(""));
        const num2 = parseInt(this.state.secondNum.join(""));

        switch(this.state.operator){
            case '+':
                return num1+num2;
            case '-':
                return num1-num2;
            case '*':
                return num1*num2;
            case '/':
                if(num2 !== 0)
                    return num1/num2;
                return ' Impossible! divided by zero!!!';
            default:
                return '';
        }
    };

    render() {
        return (
        <div>
            <h1>
            Calculator
            </h1>
            <Results result={this.state}/>
            <Keyboard onClickNumber={this.renderNumb} onClickOperator={this.renderOp}/>
        </div>
        );
    }
}

export default Calculator;