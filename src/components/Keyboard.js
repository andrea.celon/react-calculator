import React from 'react';
import Numpad from './Numpad';
import Operator from './Operator';

class Keyboard extends React.Component{
     state = {
         numbers : [],
         operator: ''
     };

     onClickNumber = async term => {
         const newNumbers = [ ...this.state.numbers, term ];
         await this.setState({ numbers : newNumbers });
         await this.props.onClickNumber(this.state.numbers);

     }

     onClickOperator = async term => {
         await this.setState({ operator : term, numbers: [] });
         await this.props.onClickOperator(this.state.operator);
     }

    render(){
        return(
            <div>
                <Numpad onClickButtonKey={this.onClickNumber}/>
                <Operator onClickButtonKey={this.onClickOperator} />
            </div>
        )

    };
}

export default Keyboard;