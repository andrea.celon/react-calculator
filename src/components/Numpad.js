import React from 'react';

class Numpad extends React.Component {

    onClickButton = (numero) => {
        this.props.onClickButtonKey(numero);
     };

    render(){
        return(
            <div>
                <button onClick={ () => this.onClickButton(1)}>1</button>
                <button onClick={ () => this.onClickButton(2)}>2</button>
                <button onClick={ () => this.onClickButton(3)}>3</button>
                <br/>
                <button onClick={ () => this.onClickButton(4)}>4</button>
                <button onClick={ () => this.onClickButton(5)}>5</button>
                <button onClick={ () => this.onClickButton(6)}>6</button>
                <br/>
                <button onClick={ () => this.onClickButton(7)}>7</button>
                <button onClick={ () => this.onClickButton(8)}>8</button>
                <button onClick={ () => this.onClickButton(9)}>9</button>
                <br/>
                <button onClick={ () => this.onClickButton(0)}>0</button>
            </div>
        )

    };
}

export default Numpad;