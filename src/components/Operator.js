import React from 'react';

class Operator extends React.Component {

    onClickButton = (operationString) => {
        this.props.onClickButtonKey(operationString);;
    };

    render(){
        return(
            <div>
                <br/>
                <button onClick={() => this.onClickButton('+')}>+</button>
                <button onClick={() => this.onClickButton('-')}>-</button>
                <br/>
                <button onClick={() => this.onClickButton('*')}>x</button>
                <button onClick={() => this.onClickButton('/')}>/</button>
                <br/>
                <button onClick={() => this.onClickButton('=')}>=</button>
            </div>
        )

    };
}

export default Operator;