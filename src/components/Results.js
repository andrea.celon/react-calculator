import React from 'react';

class Results extends React.Component {

    renderResult = () => {
        return (
                `${this.props.result.firstNum.join("")} ` +
                `${this.props.result.operator} ` +
                `${this.props.result.secondNum.join("")} ` +
                `${this.props.result.result}`
        );
    };

    render(){
        return(
            <div>
                <h2>
                    {this.renderResult()}
                </h2>
                <br/>
            </div>
        )

    };
}

export default Results;